<?php 
include('top.php');
$rmail = $_REQUEST['email'];
$rnick = $_REQUEST['nickname'];
$rpass = $_REQUEST['passw'];

function registerProcess ($email, $nick, $passw) {
	try {
		if (emailProcess($email)) {
			if (nickProcess($nick)) {
				if (passProcess($passw)) {
					$passw = password_hash($passw, PASSWORD_BCRYPT);
					if (insertData($email, $nick, $passw)) {
						//header('Location: /');
						echo 'Ok';
					} else {
						throw Exception('Problema en ultimo paso');
					}
				}
			}
		}
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}


function emailProcess($email) {
		$email = filter_var($email, FILTER_VALIDATE_EMAIL);
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$stmt = $db->prepare("SELECT email FROM users WHERE email = ?");
		$stmt->bind_param("s", $email);

		$stmt->execute();
		$stmt->bind_result($mail);

		while ($stmt -> fetch()) {
			echo 'Lala';
		}

		$stmt->close();
		$db->close();

}

function nickProcess($nick) {
	if ((strlen($nick) >= 5) && (strlen($nick) <= 20)) {
		if (!preg_match('/[^A-Za-z0-9]/', $nick)) {
			try {
				$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

				$stmt = $db->prepare("SELECT nickname FROM users WHERE nickname = ?");
				$stmt->bind_param("s", $nick);

				$stmt->execute();

				$result = $stmt->get_result();

				$stmt->close();
				$db->close();
				if (!$result) {
					echo 'Ok nick';
					return true;
				} else {
					throw new Exception('Nickname ya registrado');
				}
			} catch (Exception $e) {
				return $e;
			}
		} else {
			throw new Exception('Caracteres invalidos');
		}
	} else {
		throw new Exception('Cantidad erronea de caracteres');
	}
}

function passProcess($passw) {
	if ((strlen($passw) >= 6) && (strlen($passw) <= 15)) {
		if (!preg_match('/[^A-Za-z0-9]/', $passw)) {
			echo 'Ok passw';
			return true;
		} else {
			throw new Exception ('Password invalida');
		}
	}
}

function insertData($email, $nick, $passw) {
	try {
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$stmt = $db->prepare("INSERT INTO users (email, nickname, passw) VALUES (?, ?, ?)");
		$stmt->bind_param("sss", $email, $nick, $passw);
		$stmt->execute();

		$last_id = $stmt->insert_id;

		$stmt->close();
		$db->close();

		if (!$last_id) {
			throw new Exception('Error al insertar informacion');
		} else {
			sesionStart($last_id, $nick);
			return true;
		}
	} catch (Exception $e) {
		return $e;
	}
}

function sesionStart($id, $nick) {
	$_SESSION['idusers'] = $id;
	$_SESSION['nickname'] = $nick;
	return true;
}

registerProcess($rmail, $rnick, $rpass);
?>